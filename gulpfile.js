const { src, dest, series, watch } = require('gulp')
const del = require('del')
const sass = require('gulp-sass')(require('sass'))
const autoprefixer = require('gulp-autoprefixer')
const browserSync = require('browser-sync').create()
const cleanCSS = require('gulp-clean-css')
const concat = require('gulp-concat')
const nunjucks = require('gulp-nunjucks-render')
const htmlMin = require('gulp-htmlMin')
const babel = require('gulp-babel')
const notify = require('gulp-notify')
const uglify = require('gulp-uglify-es').default
const sourcemaps = require('gulp-sourcemaps')

const clean = () => {
  return del('dist');
}

const htmlMinify = () => {
  return src('src/njk/pages/*.html')
      .pipe(nunjucks({
        path: ['src/njk'],
      }))
      .pipe(htmlMin({
        collapseWhitespace: true,
      }))
      .pipe(dest('dist'))
}

const styles = () => {
  return src('src/scss/styles.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false,
    }))
    .pipe(cleanCSS({
      level: 2,
    }))
    .pipe(concat('style.min.css'))
    .pipe(browserSync.stream())
    .pipe(sourcemaps.write())
    .pipe(dest('dist/css'))
}

const script = () => {
  return src('src/js/main.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['@babel/env']
    }))
    .pipe(concat('main.min.js'))
    .pipe(uglify().on('error', notify.onError()))
    .pipe(browserSync.stream())
    .pipe(sourcemaps.write())
    .pipe(dest('dist/js'))
}

const watchFiles = () => {
  browserSync.init({
    server: {
      baseDir: 'dist'
    }
  })

  watch('src/njk/**/*.html', htmlMinify)
  watch('src/scss/**/*.scss', styles)
  watch('src/js/**/*.js', script)
  watch('dist/*.html').on('change', browserSync.reload)
}

exports.htmlMinify = htmlMinify
exports.styles = styles
exports.default = series(clean, htmlMinify, styles, script, watchFiles)
